DROP DATABASE QUIZIF;

CREATE DATABASE QUIZIF;

USE QUIZIF;

DROP TABLE IF EXISTS `questoes`;

CREATE TABLE `questoes` (
  `id` int NOT NULL AUTO_INCREMENT,
  `titulo` varchar(100) NOT NULL,
  `descricao` varchar(1024) NOT NULL,
  `disciplina` varchar(30) NOT NULL,
  `dificuldade` varchar(13) NOT NULL,
  `opcaoa` varchar(50) NOT NULL,
  `opcaob` varchar(50) NOT NULL,
  `opcaoc` varchar(50) NOT NULL,
  `opcaod` varchar(50) NOT NULL,
  `opcaoe` varchar(50) NOT NULL,
  `opcaocorreta` varchar(1) NOT NULL,
  `data_armazenamento` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
);